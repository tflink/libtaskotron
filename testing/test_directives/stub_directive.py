# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from libtaskotron.directives import BaseDirective

class StubDirective(BaseDirective):
    def process(self, command, input_data, env_data):
        return "this is just a stub"
