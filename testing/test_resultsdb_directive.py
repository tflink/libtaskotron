# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

"""Unit tests for libtaskotron/directives/resultsdb_directive.py"""

import os
import pytest
from copy import deepcopy
from dingus import Dingus, exception_raiser

from libtaskotron.directives import resultsdb_directive
from libtaskotron.exceptions import TaskotronDirectiveError, TaskotronValueError

from libtaskotron import check
from libtaskotron import config

from resultsdb_api import ResultsDBapiException


@pytest.mark.usefixtures('setup')
class TestResultsdbReport():

    @pytest.fixture
    def setup(self, monkeypatch):
        '''Run this before every test invocation'''

        self.cd = check.CheckDetail(
                    item = 'foo_bar',
                    report_type = check.ReportType.KOJI_BUILD,
                    outcome = 'NEEDS_INSPECTION',
                    summary = 'foo_bar summary',
                    output = ["foo\nbar"],
                    keyvals = {"foo": "moo1", "bar": "moo2"},
                )

        self.tap = check.export_TAP(self.cd)

        self.ref_input = {'results': self.tap}
        self.ref_envdata = {
                'resultsdb_job_id': 1,
                'checkname': 'test_resultsdb_report',
                'jobid': 'all/123',
                'uuid': 'c25237a4-b6b3-11e4-b98a-3c970e018701',
                'artifactsdir': '/some/directory/',
                }

        self.ref_resultdata = {u'id': 1234}

        self.ref_jobid = 1234
        self.ref_refurl = u'http://example.com/%s' % self.ref_envdata['checkname']
        self.ref_jobdata = { u'end_time': None,
                        u'href': u'http://127.0.0.1/api/v1.0/jobs/%d' % self.ref_jobid,
                        u'id': self.ref_jobid,
                        u'name': self.ref_envdata['checkname'],
                        u'ref_url': self.ref_refurl,
                        u'results': [],
                        u'results_count': 0,
                        u'start_time': None,
                        u'status': u'SCHEDULED'}

        self.stub_rdb = Dingus('resultsdb', get_testcase__returns={},
                               create_job__returns=self.ref_jobdata,
                               create_result__returns=self.ref_resultdata,
                               )
        self.test_rdb = resultsdb_directive.ResultsdbDirective(self.stub_rdb)

        # while it appears useless, this actually sets config in several tests
        monkeypatch.setattr(config, '_config', None)
        self.conf = config.get_config()
        self.conf.reporting_enabled = True
        self.conf.report_to_resultsdb = True


    def test_config_reporting_disabled(self):
        """Checks config options can disable reporting."""
        conf = config.get_config()

        for r_e, r_t_r in ((False, False), (False, True), (True, False)):
            conf.reporting_enabled = r_e
            conf.report_to_resultsdb = r_t_r

            tap = self.test_rdb.process(self.ref_input, self.ref_envdata)
            cds = check.import_TAP(tap)
            # return value should be the same TAP
            assert len(cds) == 1
            assert cds[0].__dict__ == self.cd.__dict__

        # no call should have been made
        assert len(self.stub_rdb.calls()) == 0

        config._config = None

    def test_missing_checkname(self):
        """Checks if missing checkname raises exception"""
        with pytest.raises(TaskotronDirectiveError):
            envdata = deepcopy(self.ref_envdata)
            del(envdata['checkname'])

            self.test_rdb.process(self.ref_input, envdata)

    def test_failed_tap_import(self, monkeypatch):
        """Checks if failed TAP import raises exception"""
        monkeypatch.setattr(check, 'import_TAP',
                            exception_raiser(TaskotronValueError("Testing Error")))

        with pytest.raises(TaskotronDirectiveError):
            self.test_rdb.process(self.ref_input, self.ref_envdata)

    def test_tap_missing_item(self):
        """Checks if missing item raises exception"""
        tap = ""
        for line in self.ref_input['results'].split('\n'):
            if 'item:' in line:
                continue
            tap += line + '\n'

        with pytest.raises(TaskotronDirectiveError):
            self.test_rdb.process({"results": tap}, self.ref_envdata)

    def test_tap_missing_type(self):
        """Checks if missing type raises exception"""
        tap = ""
        for line in self.ref_input['results'].split('\n'):
            if 'type:' in line:
                continue
            tap += line + '\n'

        with pytest.raises(TaskotronDirectiveError):
            self.test_rdb.process({"results": tap}, self.ref_envdata)

    def test_report(self):
        """Checks whether TAP is correctly mapped to the reporting method's
        arguments."""

        self.test_rdb.process(self.ref_input, self.ref_envdata)

        # Given the input data, the resultsdb should be called once, and only
        #   once, calling "create_result".
        # This assert failing either means that more calls were added in the
        #   source code, or that a bug is present, and "create_result" is
        #   called multiple times.
        # we expect rdb to be called 4 times: create job, update to RUNNING,
        # check for testcase, report result and complete job

        assert len(self.stub_rdb.calls()) == 5

        # Select the first call of "create_result" method.
        # This could be written as self.stub_rdb.calls()[0] at the moment, but
        #   this is more future-proof, and accidental addition of resultsdb
        #   calls is handled by the previous assert.
        call = [call for call in self.stub_rdb.calls() if call[0] == 'create_result'][0]
        # Select the keyword arguments of that call
        call_data = call[2]

        # the log url depends on the env_data, so construct it here
        ref_builder, ref_jobid = self.ref_envdata['jobid'].split('/')
        ref_log_url = '%s/builders/%s/builds/%s/steps/%s/logs/stdio' %\
                      (self.conf.taskotron_master, ref_builder, ref_jobid,
                       self.conf.buildbot_task_step)

        assert call_data['job_id'] == self.ref_jobid
        assert call_data['testcase_name'] == self.ref_envdata['checkname']
        assert call_data['outcome'] == self.cd.outcome
        assert call_data['summary'] == self.cd.summary
        assert call_data['log_url'] == ref_log_url
        assert call_data['item'] == self.cd.item
        assert call_data['type'] == self.cd.report_type

        assert 'output' not in call_data.keys()

        for key in self.cd.keyvals.keys():
            assert call_data[key] == self.cd.keyvals[key]

    def test_get_artifact_path(self, monkeypatch):
        artifactsdir = '/path/to/artifacts'
        rel_artifact = 'some/relative/file.log'
        abs_artifact = os.path.join(artifactsdir, rel_artifact)

        monkeypatch.setattr(resultsdb_directive.os.path, 'exists', lambda x: True)
        assert self.test_rdb.get_artifact_path(artifactsdir, rel_artifact) == rel_artifact
        assert self.test_rdb.get_artifact_path(artifactsdir, abs_artifact) == rel_artifact

    def test_get_artifact_path_errors(self, monkeypatch):
        artifactsdir = '/path/to/artifacts'
        artifact = '/some/absolute/file.log'

        # if file does not exist None is expected
        monkeypatch.setattr(resultsdb_directive.os.path, 'exists', lambda x: False)
        assert self.test_rdb.get_artifact_path(artifactsdir, artifact) is None

        # if file exists, but is out of the artifactsdir, None is expected
        monkeypatch.setattr(resultsdb_directive.os.path, 'exists', lambda x: True)
        assert self.test_rdb.get_artifact_path(artifactsdir, artifact) is None

    def test_report_artifact_in_log_url(self, monkeypatch):
        """Checks whether artifact is correctly mapped to log_url"""
        cd = check.CheckDetail(
            item='foo_bar',
            report_type=check.ReportType.KOJI_BUILD,
            outcome='NEEDS_INSPECTION',
            summary='foo_bar summary',
            output=["foo\nbar"],
            keyvals={"foo": "moo1", "bar": "moo2"},
            artifact='digest/logs/logfile.log'
            )
        monkeypatch.setattr(self.test_rdb, 'get_artifact_path', lambda *x: 'digest/logs/logfile.log')

        tap = check.export_TAP(cd)
        ref_input = {'results': tap}

        self.test_rdb.process(ref_input, self.ref_envdata)

        # Given the input data, the resultsdb should be called once, and only
        #   once, calling "create_result".
        # This assert failing either means that more calls were added in the
        #   source code, or that a bug is present, and "create_result" is
        #   called multiple times.
        # we expect rdb to be called 4 times: create job, update to RUNNING,
        # check for testcase, report result and complete job

        assert len(self.stub_rdb.calls()) == 5

        # Select the first call of "create_result" method.
        # This could be written as self.stub_rdb.calls()[0] at the moment, but
        #   this is more future-proof, and accidental addition of resultsdb
        #   calls is handled by the previous assert.
        call = [call for call in self.stub_rdb.calls() if call[0] == 'create_result'][0]
        # Select the keyword arguments of that call
        call_data = call[2]

        # the log url depends on the env_data, so construct it here
        ref_log_url = '%s/all/%s/task_output/%s' %\
                      (self.conf.artifacts_baseurl, self.ref_envdata['uuid'], cd.artifact)

        assert call_data['log_url'] == ref_log_url

    def test_create_job(self):
        # make sure that the proper API calls are made to create a resultsdb job
        test_jobdata = self.test_rdb.create_resultsdb_job(self.ref_envdata['checkname'],
                                                          self.ref_refurl)

        assert test_jobdata == self.ref_jobdata

        rdb_calls = self.stub_rdb.calls()
        # we expect only one call to resultsdb when creating a job
        assert len(rdb_calls) == 2
        assert rdb_calls[0][0] == 'create_job'
        assert rdb_calls[1][0] == 'update_job'

    def test_complete_job(self):
        # make sure that the proper API calls are made to complete the job
        self.test_rdb.complete_resultsdb_job(self.ref_jobid)

        rdb_calls = self.stub_rdb.calls()
        # we expect only one call to resultsdb when completing a job
        assert len(rdb_calls) == 1
        assert rdb_calls[0][0] == 'update_job'
        assert rdb_calls[0][2]['status'] == 'COMPLETED'

    def test_ensure_testcase_creation_notexist(self):
        # check to see if the testcase is created in the case that it doesn't
        # already exist

        self.stub_rdb.get_testcase = exception_raiser(ResultsDBapiException('Testcase not found'))

        self.test_rdb.ensure_testcase_exists(self.ref_envdata['checkname'])

        rdb_calls = self.stub_rdb.calls()

        # dingus doesn't record calls to exception_raiser
        assert len(rdb_calls) == 1
        assert rdb_calls[0][0] == 'create_testcase'

    def test_ensure_testcase_creation_exists(self):
        # check to make sure that a testcase is _not_ created in the case that
        # it already exists

        self.test_rdb.ensure_testcase_exists(self.ref_envdata['checkname'])

        rdb_calls = self.stub_rdb.calls()

        assert len(rdb_calls) == 1
        assert rdb_calls[0][0] == 'get_testcase'

    def test_ensure_testcase_cached(self):
        # check to make sure that ensure_testcase_exists does not query resultsdb unnecessarily

        self.test_rdb.ensure_testcase_exists(self.ref_envdata['checkname'])
        self.test_rdb.ensure_testcase_exists(self.ref_envdata['checkname'])

        rdb_calls = self.stub_rdb.calls()

        assert len(rdb_calls) == 1
        assert rdb_calls[0][0] == 'get_testcase'

    def test_tap_output(self):
        """Checks whether TAP is correctly mapped to the reporting method's
        arguments."""

        output = self.test_rdb.process(self.ref_input, self.ref_envdata)
        data = check.import_TAP(output)

        assert data[0]._internal['resultsdb_result_id'] == 1234
