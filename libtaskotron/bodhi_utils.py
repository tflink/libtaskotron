# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Utility functions for dealing with Bodhi'''

from __future__ import absolute_import
import fedora.client

from .logger import log
from . import rpm_utils
from . import python_utils
from . import exceptions as exc
from . import config


class BodhiUtils(object):
    '''Helper Bodhi methods.

    :ivar fedora.client.BodhiClient client: Bodhi client instance
    '''

    #: How many requests to make in a single time when using "multicall"-like
    #: Bodhi methods. '50' is a good size, there is a 2 minute timeout and it
    #: usually takes about 20 seconds for Bodhi to evaluate 50 requests, so
    #: there's still a lot of buffer (increasing the size further doesn't
    #: seem to speed it up noticeably)
    _MULTICALL_REQUEST_SIZE = 50

    def __init__(self, client=None):
        '''Create a new BodhiUtils instance.

        :param client: custom :class:`BodhiClient` instance. If ``None``, a
                       default BodhiClient instance is used.
        '''

        self.config = config.get_config()

        if not client:
            log.debug('Creating Bodhi client to %s' % self.config.bodhi_server)
            self.client = fedora.client.bodhi.BodhiClient(base_url=self.config.bodhi_server,
                                                          username=self.config.fas_username,
                                                          password=self.config.fas_password)
            # automatically retry failed requests (HTTP 5xx and similar)
            self.client.retries = 10
        else:
            self.client = client

    def query_update(self, package):
        '''Get the last Bodhi update matching criteria.

        :param str package: package NVR or package name or update title or
                            update ID

                            .. note::
                             Only NVR allowed, not NEVR. See
                             https://fedorahosted.org/bodhi/ticket/592.
        :return: most recent Bodhi update object matching criteria, or ``None``
                 when no update is found
        :rtype: :class:`bunch.Bunch`
        '''
        log.debug('Searching Bodhi updates for: %s', package)
        res = self.client.query(package=package, limit=1)
        if res['updates']:
            return res['updates'][0]
        else:
            return None

    def build2update(self, builds, strict=False):
        '''Find matching Bodhi updates for provided builds.

        :param builds: builds to search for in N(E)VR format (``foo-1.2-3.fc20``
                       or ``foo-4:1.2-3.fc20``)
        :type builds: iterable of str
        :param bool strict: if ``False``, incomplete Bodhi updates are allowed.
                            If ``True``, every Bodhi update will be compared
                            with the set of provided builds. If there is an
                            Bodhi update which contains builds not provided in
                            ``builds``, that update is marked as incomplete and
                            removed from the result - i.e. all builds from
                            ``builds`` that were part of this incomplete update
                            are placed in the second dictionary of the result
                            tuple.
        :return: a tuple of two dictionaries:

                 * The first dict provides mapping between ``builds`` and their
                   updates where no error occured.

                   ``{build (string): Bodhi update (Bunch)}``
                 * The second dict provides mapping between ``builds`` and their
                   updates where some error occured. The value is ``None`` if
                   the matching Bodhi update could not be found (the only
                   possible cause of failure if ``strict=False``). Or the value
                   is a Bodhi update that was incomplete (happens only if
                   ``strict=True``).

                   ``{build (string): Bodhi update (Bunch) or None}``
                 * The set of keys in both these dictionaries correspond exactly
                   to ``builds``. For every build provided in ``builds`` you'll
                   get an answer in either the first or the second dictionary,
                   and there will be no extra builds that you haven't specified.
        :raise TaskotronValueError: if ``builds`` type is incorrect
        '''
        # validate input params
        if not python_utils.iterable(builds, basestring):
            raise exc.TaskotronValueError(
                "Param 'builds' must be an iterable of strings, and yours was: %s" % type(builds))

        updates = []
        build2update = {}
        failures = {}
        # Bodhi works with NVR only, but we have to ensure we receive and return
        # even NEVR format. So we need to convert internally.
        builds_nvr = set([rpm_utils.rpmformat(build, 'nvr') for build in builds])
        builds_queue = list(builds_nvr)

        log.info('Querying Bodhi to map %d builds to their updates...', len(builds))

        # retrieve all update data
        while builds_queue:
            builds_chunk = builds_queue[:self._MULTICALL_REQUEST_SIZE]
            builds_chunk = ' '.join(builds_chunk)
            # this method doesn't seem to be exposed or documented anywhere, it's
            # just available in the Bodhi source code:
            # https://github.com/fedora-infra/bodhi/blob/master/bodhi/controllers.py#L202-L220
            # For a space-delimited list of builds in format of NVR, it returns a
            # dictionary in format of {build: update_data}.
            res = self.client.send_request(method='get_updates_from_builds',
                                           req_params={'builds': builds_chunk})
            # there is a bug - it contains an extra item "tg_flash=None"
            # https://github.com/fedora-infra/bodhi/issues/152
            res.pop('tg_flash', None)

            updates.extend(res.values())
            builds_queue = builds_queue[self._MULTICALL_REQUEST_SIZE:]

            # don't query for builds which were already found
            for update in res.values():
                for build in update['builds']:
                    if build['nvr'] in builds_queue:
                        builds_queue.remove(build['nvr'])

            log.info('Bodhi queries done: %d/%d', len(builds_nvr)-len(builds_queue),
                     len(builds_nvr))

        # separate builds into OK and failures
        for update in updates:
            # all builds listed in the update
            bodhi_builds = set([build['nvr'] for build in update['builds']])
            # builds *not* provided in @param builds but part of the update (NVRs)
            missing_builds = bodhi_builds.difference(builds_nvr)
            # builds provided in @param builds and part of the update
            matched_builds = [build for build in builds if
                              rpm_utils.rpmformat(build, 'nvr') in bodhi_builds]

            # reject incomplete updates when strict
            if missing_builds and strict:
                for build in matched_builds:
                    failures[build] = update
                continue

            # otherwise the update is complete or we don't care
            for build in matched_builds:
                build2update[build] = update

        # mark builds without any associated update as a failure
        for build in builds:
            if build not in build2update and build not in failures:
                failures[build] = None

        diff = set(builds).symmetric_difference(
            set(build2update.keys()).union(set(failures.keys())))
        assert not diff, "Returned NVRs different from input NVRs: %s" % diff

        return (build2update, failures)
