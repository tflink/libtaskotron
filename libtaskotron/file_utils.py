# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from __future__ import absolute_import
import os
import sys
import urllib2
import urlgrabber.grabber
import urlgrabber.progress
from libtaskotron.logger import log
from libtaskotron.exceptions import TaskotronRemoteError


def makedirs(fullpath):
    '''This is the same as :meth:`os.makedirs`, but does not raise an exception
    when the destination directory already exists.

    :raise OSError: if directory doesn't exist and can't be created
    '''
    try:
        os.makedirs(fullpath)
        assert os.path.isdir(fullpath)
    except OSError, e:
        if e.errno == 17: # "[Errno 17] File exists"
            # if it is a directory everything is ok
            if os.path.isdir(fullpath):
                return
            # otherwise it is a file/socket/etc and it is an error
            else:
                log.warn("Can't create directory, something else already exists: %s",
                         fullpath)
                raise
        else:
            log.warn("Can't create directory: %s", fullpath)
            raise


def get_urlgrabber(**kwargs):
    '''Get a new instance of :class:`URLGrabber` that is reasonably configured
       (progress bar when in terminal, automatic retries, connection timeout)

       :param kwargs: additional kwargs to add to the URLGrabber constructor
       :return: :class:`urlgrabber.grabber.URLGrabber` instance
    '''
    grabber_args = {'retry': 3, 'timeout': 120}
    grabber_args.update(kwargs)

    grabber = urlgrabber.grabber.URLGrabber(**grabber_args)

    # progress bar when in terminal
    if hasattr(sys.stdout, "fileno") and os.isatty(sys.stdout.fileno()):
        grabber.opts.progress_obj = urlgrabber.progress.TextMeter()

    # retry on socket timeout
    if 12 not in grabber.opts.retrycodes:
        grabber.opts.retrycodes.append(12)

    return grabber


def _same_length(filepath, url):
    '''Determine whether a local file and a file referred by HTTP URL have the
    same length. If any exception occurs, ``False`` is returned.
    :rtype: bool
    '''
    try:
        local_size = os.path.getsize(filepath)

        remote_file = urllib2.urlopen(url, timeout=120)
        remote_file.close()
        remote_size = int(remote_file.headers['Content-Length'])

        return local_size == remote_size
    except:
        return False


def download(url, dirname, filename=None, cachedir=None, grabber=None):
    '''Download a file. Resuming partially downloaded files is supported.

    :param str url: file URL to download
    :param str dirname:  directory path; if the directory does not exist, it gets
                         created (and all its parent directories).
    :param str filename: name of downloaded file; if not provided, the basename
                         is extracted from URL
    :param str cachedir: If set, the file will be downloaded to a cache
                         directory specified by this parameter. If the file is
                         already present and of the same length, download is skipped.
                         The requested destination file (``dirname/filename``)
                         will be a symlink to the cached file.
                         This directory is automatically created if not present.
    :param grabber: custom :class:`urlgrabber.grabber.URLGrabber` instance to be
                    used for actual downloading
    :return: the path to the downloaded file
    :rtype: str
    :raise TaskotronRemoteError: if download fails
    '''

    if not grabber:
        grabber = get_urlgrabber()

    if not filename:
        filename = os.path.basename(url)

    dl_dest = dest = os.path.abspath(os.path.join(dirname, filename))
    dirs_to_create = [dirname]

    if cachedir:
        dl_dest = os.path.join(cachedir, filename)
        dirs_to_create.append(cachedir)

    for directory in dirs_to_create:
        makedirs(directory)

    # check file existence and validity
    download = True
    if os.path.exists(dl_dest):
        if _same_length(dl_dest, url):
            log.debug('Already downloaded: %s', dl_dest)
            download = False
        else:
            log.debug('Cached file %s differs from its online version. '
                      'Redownloading.', dl_dest)

    # download the file
    if download:
        log.debug('Downloading%s: %s', ' (cached)' if cachedir else '', url)
        try:
            grabber.urlgrab(url=url, filename=dl_dest)
        except urlgrabber.grabber.URLGrabError, e:
            log.exception('Download failed for: %s', url)
            # the file can be incomplete, remove
            if os.path.exists(dl_dest):
                try:
                    os.remove(dl_dest)
                except OSError, e:
                    log.exception('Could not delete incomplete file: %s', dl_dest)
            raise TaskotronRemoteError(e)

    # create a symlink if the download was cached
    if cachedir:
        try:
            if (os.path.exists(dest)):
                # if there already is something at the destination, we need to
                # remove it first
                os.remove(dest)
            os.symlink(dl_dest, dest)
        except OSError, e:
            log.exception("Can't create symlink %s -> %s", dl_dest, dest)
            raise

    return dest
