# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Methods for operating with a task formula'''

from __future__ import absolute_import
import collections
import string

import yaml
from libtaskotron import exceptions as exc
from libtaskotron.logger import log


def parse_yaml_from_file(filename):
    with open(filename, 'r') as datafile:
        return parse_yaml(datafile.read())


def parse_yaml(contents):
    return yaml.safe_load(contents)


def _replace_vars(text, variables):
    '''Go through ``text`` and replace all variables (in the form of what is
    supported by :class:`string.Template`) with their values, as provided in
    ``variables``. This is used for variable expansion in the task formula.

    :param str text: input text where to search for variables
    :param dict variables: names (keys) and values of variables to replace
    :return: if ``text`` contains just a single variable and nothing else, the
             variable value is directly returned (i.e. with matching type, not
             cast to ``str``). If ``text`` contains something else as well
             (other variables or text), a string is returned.
    :raise TaskotronYamlError: if ``text`` contains a variable that is not
                               present in ``variables``, or if the variable
                               syntax is incorrect
    '''

    try:
        # try to find the first match
        match = string.Template.pattern.search(text)

        if not match:
            return text

        # There are 4 groups in the pattern: 1 - escaped, 2 - named, 3 - braced,
        # 4 - invalid. Group 0 returns the whole match.
        if match.group(0) == text and (match.group(2) or match.group(3)):
            # We found a single variable and nothing more. We shouldn't return
            # a string, but the exact value, so that we don't lose value type.
            # This makes it possible to pass lists, dicts, etc as variables.
            var_name = match.group(2) or match.group(3)
            return variables[var_name]

        # Now it's clear there's also something else in `text` than just a
        # single variable. We will replace all variables and return a string
        # again.
        output = string.Template(text).substitute(variables)
        return output

    except KeyError as e:
        raise exc.TaskotronYamlError("The task formula includes a variable, "
            "but no value has been provided for it: %s" % e)

    except ValueError as e:
        raise exc.TaskotronYamlError("The task formula includes an incorrect "
            "variable definition. Dollar signs must be doubled if they "
            "shouldn't be considered as a variable denotation.\n"
            "Error: %s\n"
            "Text: %s" % (e, text))


def replace_vars_in_action(action, variables):
    '''Find all variables that are leaves (in a tree sense) in an action. Leaves
    are variables which can no longer be traversed, i.e. "primitive" types like
    ``str`` or ``int``. Non-leaves are containers like ``dict`` or ``list``.

    For all leaves, call :func:`_replace_vars` and update their value with
    the function's output.

    :param dict action: An action specification parsed from the task formula.
                        See :meth:`.Runner.do_actions` to see what an action
                        looks like.
    :param dict variables: names (keys) and values of variables to replace
    :raise TaskotronYamlError: if ``text`` contains a variable that is not
                               present in ``variables``, or if the variable
                               syntax is incorrect
    '''

    visited = []  # all visited nodes in a tree
    stack = [action]  # nodes waiting for inspection

    while stack:
        vertex = stack.pop()

        if vertex in visited:
            continue

        visited.append(vertex)
        children = [] # list of tuples (index/key, child_value)

        if isinstance(vertex, collections.MutableMapping):
            children = vertex.items()  # list of (key, value)
        elif isinstance(vertex, collections.MutableSequence):
            children = list(enumerate(vertex))  # list of (index, value)
        else:
            log.warn("Unknown structure '%s' in YAML file, this shouldn't "
                     "happen: %s", type(vertex), vertex)

        for index, child_val in children:
            if isinstance(child_val, basestring):
                # leaf node and a string, replace variables
                vertex[index] = _replace_vars(child_val, variables)
            elif isinstance(child_val, collections.Iterable):
                # traversable further down, mark for visit
                stack.append(child_val)
