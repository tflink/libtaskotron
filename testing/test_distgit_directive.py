# -*- coding: utf-8 -*-
# Copyright 2009-2015, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

import os.path
import pytest
from dingus import Dingus

from libtaskotron import file_utils
from libtaskotron.directives import distgit_directive
import libtaskotron.exceptions as exc


class TestDistGitDirective():
    def setup_method(self, method):
        self.ref_nvr = 'foo-1.2-3.fc99'
        self.ref_path = ['foo.spec']
        self.ref_localpath = ['local.foo.spec']
        self.ref_branch = 'f99'
        self.ref_envdata = {'workdir': '/var/tmp/foo'}
        self.helper = distgit_directive.DistGitDirective()

    def _get_url(self, path):
        return distgit_directive.URL_FMT % ('foo', path, self.ref_branch)

    def test_parse_download_command(self, monkeypatch):
        ref_input = {'action': 'download',
                     'package': self.ref_nvr,
                     'path': self.ref_path}

        monkeypatch.setattr(file_utils, 'download', Dingus())

        self.helper.process(ref_input, self.ref_envdata)

        download_calls = file_utils.download.calls

        assert len(download_calls) == 1
        assert download_calls[0][1][0] == self._get_url(self.ref_path[0])
        assert download_calls[0][1][2] == os.path.join(self.ref_envdata['workdir'], self.ref_path[0])

    def test_parse_download_command_localpath(self, monkeypatch):
        ref_input = {'action': 'download',
                     'package': self.ref_nvr,
                     'path': self.ref_path,
                     'localpath': self.ref_localpath}

        monkeypatch.setattr(file_utils, 'download', Dingus())

        self.helper.process(ref_input, self.ref_envdata)

        download_calls = file_utils.download.calls

        assert len(download_calls) == 1
        assert download_calls[0][1][0] == self._get_url(self.ref_path[0])
        assert download_calls[0][1][2] == os.path.join(self.ref_envdata['workdir'], self.ref_localpath[0])

    def test_parse_download_command_multiple_localpath(self, monkeypatch):
        ref_path = ['file1', 'file2']
        ref_localpath = ['file3', 'file4']
        ref_input = {'action': 'download',
                     'package': self.ref_nvr,
                     'path': ref_path,
                     'localpath': ref_localpath}

        monkeypatch.setattr(file_utils, 'download', Dingus())

        self.helper.process(ref_input, self.ref_envdata)

        download_calls = file_utils.download.calls

        assert len(download_calls) == 2
        assert download_calls[0][1][0] == self._get_url(ref_path[0])
        assert download_calls[1][1][0] == self._get_url(ref_path[1])
        assert download_calls[0][1][2] == os.path.join(self.ref_envdata['workdir'], ref_localpath[0])
        assert download_calls[1][1][2] == os.path.join(self.ref_envdata['workdir'], ref_localpath[1])

    def test_parse_download_command_incorrect_localpath(self):
        ref_path = ['file1', 'file2']
        ref_localpath = ['file3']
        ref_input = {'action': 'download',
                     'package': self.ref_nvr,
                     'path': ref_path,
                     'localpath': ref_localpath}

        with pytest.raises(exc.TaskotronValueError):
            self.helper.process(ref_input, self.ref_envdata)

    def test_parse_download_command_incorrect_path_str(self):
        ref_path = 'file1'
        ref_input = {'action': 'download',
                     'package': self.ref_nvr,
                     'path': ref_path}

        with pytest.raises(exc.TaskotronValueError):
            self.helper.process(ref_input, self.ref_envdata)

    def test_parse_download_command_incorrect_localpath_str(self):
        ref_localpath = 'file2'
        ref_input = {'action': 'download',
                     'package': self.ref_nvr,
                     'path': self.ref_path,
                     'localpath': ref_localpath}

        with pytest.raises(exc.TaskotronValueError):
            self.helper.process(ref_input, self.ref_envdata)

    def test_parse_download_command_missing_package(self):
        ref_input = {'action': 'download',
                     'path': self.ref_path}

        with pytest.raises(exc.TaskotronDirectiveError):
            self.helper.process(ref_input, self.ref_envdata)

    def test_parse_download_command_missing_path(self):
        ref_input = {'action': 'download',
                     'package': self.ref_nvr}

        with pytest.raises(exc.TaskotronDirectiveError):
            self.helper.process(ref_input, self.ref_envdata)
