# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing
from __future__ import absolute_import

DOCUMENTATION = """
module: bodhi_comment_directive
short_description: create a comment for a Bodhi update
description: |
  Take TAP formmated check results, find all `Bodhi`_ update results in it,
  and post a comment under each of these updates stating the result of the
  check.

  .. _Bodhi: https://admin.fedoraproject.org/updates/

  The comment will roughly look like this::

    Taskotron: rpmlint test PASSED on x86_64. Result log: <url>

  .. note:: You have to have ``fas_username`` and ``fas_password`` configured
    in your config file, otherwise the comments won't be posted.

  .. note:: The <url> will point to the Buildbot's full stdio log.
    It is possible to make the <url> point to the respective ResultsDB's
    result page, if you use the ``resultsdb_directive``'s exported output
    as an input for this directive (see Examples section).
parameters:
  results:
    required: true
    description: single or multiple check results in TAP13 format
    type: str
  doreport:
    required: true
    description: |
      select ``all`` to always post the check result, regardless of
      previoust Taskotron comments for the same update. Select ``onchange`` to
      only post comments if the check result status changed (e.g. from ``FAILED``
      to ``PASSED``), or if the ``FAILED`` status repeats itself **and** long
      enough time has passed between these two failures (configurable as
      ``bodhi_posting_comments_span`` in the config file).

      For production purposes you usually want to use ``onchange``, for
      development purposes you might want to use ``all``.
    type: str
    choice: [all, onchange]
returns: |
  A list of bool values. The number and order of the bool values matches the
  number and order of Bodhi update check results in ``results``. The bool value
  is ``True`` if comment was posted successfully or comment wasn't meant to be
  posted (either posting is turned off or comment was already posted), ``False``
  if some problem was encountered.
raises: |
  * :class:`.TaskotronDirectiveError`: if ``results`` is not a string in valid
    TAP13 format
  * :class:`.TaskotronValueError`: if ``results`` refer to a Bodhi update which
    can't be found in Bodhi
version_added: 0.4
"""

EXAMPLES = """
Run ``upgradepath`` check and then report all results to Bodhi (using
``onchange`` approach)::

    - name: run upgradepath
      python:
          file: upgradepath.py
          callable: main
          custom_args: [--debug, "${koji_tag}"]
      export: upgradepath_output

    - name: report results to Bodhi
      bodhi_comment:
          results: ${upgradepath_output}
          doreport: 'onchange'

Run ``upgradepath`` check, report results to ResultsDB and then to
Bodhi (using ``onchange`` approach). This also causes the url of the
logs in the Bodhi comment to point to the ResultsDB's respective
result page, instead of the full stdio log.::

    - name: run upgradepath
      python:
          file: upgradepath.py
          callable: main
          custom_args: [--debug, "${koji_tag}"]
      export: upgradepath_output

    - name: report results to ResultsDB
      resultsdb:
         results: ${upgradepath_output}
      export: ${resultsdb_output}

    - name: report results to Bodhi
      bodhi_comment:
          results: ${resultsdb_output}
          doreport: 'onchange'
"""

from libtaskotron.directives import BaseDirective

from libtaskotron.logger import log
from libtaskotron import check
from libtaskotron import config
from libtaskotron import bodhi_utils
from libtaskotron import buildbot_utils

from libtaskotron.exceptions import TaskotronDirectiveError, TaskotronValueError
from libtaskotron.arch_utils import basearch

import datetime
import re

RE_COMMENT = re.compile(r'Taskotron: (?P<test_name>\w+) test (?P<result>\w+)'\
                              r' on (?P<arch>\w+)\. Result log:[\t \n]+'\
                              r'(?P<result_url>[/:\w]+).*')


# symbols used for consistant declaration of state
PASS = 'state_PASS'# <%s>' % __name__
FAIL = 'state_FAIL'# <%s>'% __name__
INCOMPLETE = 'state_INCOMPLETE'# <%s>' % __name__
UNKNOWN = 'state_UNKNOWN'# <%s>' % __name__

directive_class = 'BodhiCommentDirective'

class BodhiCommentDirective(BaseDirective):

    def __init__(self, bodhi_api=None):

        if bodhi_api:
            self.bodhi_api = bodhi_api
        else:
            self.bodhi_api = bodhi_utils.BodhiUtils()

    def process(self, input_data, env_data):
        output_data = []

        Config = config.get_config()
        if not (Config.reporting_enabled and Config.report_to_bodhi):
            log.info("Reporting to Bodhi is disabled, not doing anything.")
            return

        if not ('doreport' in input_data and 'results' in input_data):
            detected_args = ', '.join(input_data.keys())
            raise TaskotronDirectiveError("The bodhi_comment directive "\
                    "requires 'doreport' and 'results' arguments. "\
                    "Detected arguments: %s" % detected_args)

        if input_data['doreport'] not in ['all', 'onchange']:
            raise TaskotronDirectiveError("The argument 'doreport' is"\
                    "set to invalid value '%s'. Valid values are: "\
                    "'all', 'onchange'.")

        if 'checkname' not in env_data:
            raise TaskotronDirectiveError("The bodhi_comment directive "\
                    "requires checkname.")

        try:
            check_details = check.import_TAP(input_data['results'])
        except TaskotronValueError as e:
            log.exception(e)
            raise TaskotronDirectiveError("Failed to load the 'results':"\
                    "%s" % e)

        # Filter the results of "BODHI_UPDATE" type
        check_details = [r for r in check_details
                if r.report_type == check.ReportType.BODHI_UPDATE]

        # ? Log when no results of the type are found ?

        _, log_url = buildbot_utils.get_urls(env_data['jobid'],
                                             Config.taskotron_master,
                                             Config.buildbot_task_step)

        log.info('Posting %s results to Bodhi...', len(check_details))

        for index, detail in enumerate(check_details, start=1):
            # Check for the presence of resultsdb's result id, and create
            # alternate log_url, than points to the respective frontend page.
            resultsdb_id = detail._internal.get('resultsdb_result_id', None)
            if resultsdb_id:
                log_url = "%s/results/%s" % (Config.resultsdb_frontend, resultsdb_id)

            outcome = _post_testresult(self.bodhi_api, detail.item,
                        env_data['checkname'],
                        detail.outcome,
                        url = log_url,
                        doreport = input_data['doreport'],
                        arch = detail.keyvals.get('arch', 'noarch'),
                        )

            if not outcome:
                log.warning("Failed to post Bodhi Comment: `%s` `%s` `%s`",
                            detail.item,
                            env_data['checkname'],
                            detail.outcome)

            output_data.append(outcome)

            # Posting to Bodhi takes a long time. We need to display some progress,
            # else our tasks might get killed due to inactivity. Print status
            # for every 20th build, or when complete.
            if index % 20 == 0 or index == len(check_details):
                log.info('Bodhi results submitted: %d/%d', index,
                         len(check_details))

        return output_data


class BodhiUpdateState:
    def __init__(self, name, kojitag='updates'):
        self.test_states = {    'depcheck_32':'NORUN',
                                'depcheck_64':'NORUN',
                                'upgradepath':'NORUN'
                           }

        # since upgradepath is only run on updates-pending,
        # set it to passed if we're working on updates-testing
        if kojitag == 'updates-testing':
            self.test_states['upgradepath'] = 'PASSED'

        self.result_history = []
        self.update_name = name
        self.test_change = False

    def num_results(self):
        ''' Retrieve the number of results currently held '''
        return len(self.result_history)

    def add_result(self, new_result):
        ''' Update the current state with a new result.

            :param dict new_result: a dictionary containing the following keys:
                ``time`` (datetime object), ``testname``, ``result``, ``arch``
        '''

        self.result_history.append(new_result)

        # copy the current test state so that we can compare later
        old_state = self.test_states.copy()

        # for depcheck, we only care about i386 and x86_64 arches
        # anything else will be ignored
        if new_result['testname'] == 'depcheck':
            arch = basearch(new_result['arch'])

            if arch == 'i386':
                self.test_states['depcheck_32'] = new_result['result']

            elif arch == 'x86_64':
                self.test_states['depcheck_64'] = new_result['result']

            else:
                log.info("Arch %s is not valid for depcheck. Ignoring." \
                    % new_result['arch'])

        # don't care about the arch for upgrade path. It should be noarch
        # but the results aren't changed if it isn't
        elif new_result['testname'] == 'upgradepath':
            self.test_states['upgradepath'] = new_result['result']

        # detect if any tests changed from the last result
        if old_state != self.test_states:
            self.test_change = True
        else:
            self.test_change = False

    def get_state(self):
        ''' Determine state based on currently known results '''
        test_values = set(self.test_states.values())
        if 'NORUN' in test_values:
            return INCOMPLETE
        if 'FAILED' in test_values:
            return FAIL
        if 'PASSED' in test_values and len(test_values) == 1:
            return PASS

        return UNKNOWN

    def get_state_str(self):
        '''Get state string description to be used in debug logs'''
        state_str = '  %s' % self.test_states
        for result in self.result_history:
            state_str += '\n  %s' % result

        return state_str

    def did_test_change(self):
        ''' Determine whether the state of any tests changed with the addition
            of the last result'''
        return self.test_change


def _is_comment_email_needed(update_name, parsed_comments, new_result,
                                   kojitag):
    '''
       Determines whether or not to send an email with the comment posted to bodhi.
       Uses previous comments on update in order to determine current state

       :param str update_name: name of the update to be tested
       :param parsed_comments: already existing Taskotron comments for
           the update
       :type parsed_comments: list of dict
       :param dict new_result: the new result to be posted
       :param str kojitag: the koji tag being tested (affects the expected tests)
       :rtype: bool
    '''

    # compute current state
    update_state = BodhiUpdateState(update_name, kojitag=kojitag)
    for result in parsed_comments:
        update_state.add_result(result)

    current_state = update_state.get_state()
    log.debug('Current state for %s: %s\n  %s', update_name, current_state,
              update_state.get_state_str())
    update_state.add_result(new_result)
    new_state = update_state.get_state()
    log.debug('New state for %s: %s\n  %s', update_name, new_state,
              update_state.get_state_str())

    # by default, we want to send email on state change, so start from there
    send_email = True

    if new_state is PASS and current_state is not FAIL:
        send_email = False

    log.debug('Bodhi email needed for %s: %s', update_name,
              'yes' if send_email else 'no')

    return send_email


def _parse_result_from_comment(comment):
    '''
       Parses timestamp and results from bodhi comment

       :param dict comment: the ``comment`` part of bodhi update object
       :rtype: dict
    '''

    comment_time = datetime.datetime.strptime(comment['timestamp'], '%Y-%m-%d %H:%M:%S')

    # comment form to match:
    # 'Taskotron: %s test %s on %s. Result log: %s'
    # note that it isn't looking for the autoqa user right now, that needs to
    # be done in any code that calls this
    comment_match = RE_COMMENT.match(comment['text'])

    test_name = ''
    result = ''
    arch = ''

    if comment_match:
        test_name = comment_match.group('test_name')
        result = comment_match.group('result')
        arch = comment_match.group('arch')
    else:
        log.warning('Failed to parse bodhi comment: %r', comment['text'])

    return {'time':comment_time, 'testname':test_name,
            'result':result, 'arch':arch}


def _already_commented(update, testname, arch):
    '''Check if Taskotron comment is already posted.

       :param update: Bodhi update object
       :param str testname: the name of the test
       :param str arch: tested architecture

       Note: Only NVR allowed, not ENVR. See https://fedorahosted.org/bodhi/ticket/592.

       :return: Tuple containing old result and time when the last comment was posted.
           If no comment is posted yet, or it is, but the update
           has been modified since, tuple will contain two empty strings.
    '''

    Config = config.get_config()

    comment_re = r'Taskotron:[\s]+%s[\s]+test[\s]+(\w+)[\s]+on[\s]+%s' % (testname, arch)
    old_result = ''
    comment_time = ''

    taskotron_comments = [comment for comment in update['comments']
                          if comment['author'] == Config.fas_username]
    for comment in taskotron_comments:
        m = re.match(comment_re, comment['text'])
        if m is None:
            continue
        old_result = m.group(1)
        comment_time = comment['timestamp']
    # check whether update was modified after the last posted comment
    if update['date_modified'] > comment_time:
        return ('','')
    return (old_result, comment_time)


def _is_comment_needed(old_result, comment_time, result, time_span = None):
    '''Check if the comment is meant to be posted.

       :param str old_result: the result of the last test
       :param str comment_time: the comment time of the last test
       :param str result: the result of the test
       :param int time_span: waiting period (in minutes) before posting the same comment

       :return: ``True`` if the comment will be posted, ``False`` otherwise.
       :rtype: bool
    '''
    # the first comment or a comment with different result, post it
    if not old_result or old_result != result:
        return True

    # If we got here, it means that the comment with the same result has been
    # already posted, we now need to determine whether we can post the
    # comment again or not.
    # If the previous result is *not* 'FAILED', we won't post it in order not to
    # spam developers.
    # If the previous result *is* 'FAILED', we will need to check whether given
    # time span expired, if so, we will post the same comment again to remind
    # a developer about the issue.

    if result != 'FAILED':
        return False

    if time_span is None:
        Config = config.get_config()
        time_span = Config.bodhi_posting_comments_span


    posted_datetime = datetime.datetime.strptime(comment_time, '%Y-%m-%d %H:%M:%S')

    delta = (datetime.datetime.utcnow() - posted_datetime)
    # total_seconds() is introduced in python 2.7, until 2.7 is everywhere...
    delta_minutes = (delta.seconds + delta.days * 24 * 3600) / 60

    if delta_minutes < time_span:
        return False

    return True


def _post_testresult(bodhi_api, update, testname, result, url,
                     arch = 'noarch', karma = 0, doreport='onchange'):
    '''Post comment and karma to bodhi

       :param str update: the **title** of the update comment on
       :param str testname: the name of the test
       :param str result: the result of the test
       :param str url: url of the result of the test
       :param str arch: tested architecture (default ``noarch``)
       :param int karma: karma points (default ``0``)
       :param str doreport: set to 'all' to force posting bodhi comment

       :return: ``True`` if comment was posted successfully or comment wasn't
           meant to be posted (either posting is turned off or comment was
           already posted), ``False`` otherwise.
       :rtype: bool
    '''

    # TODO when new bodhi releases, add update identification by UPDATEID support

    Config = config.get_config()

    if not update or not testname or not result or url == None:
        log.error("bodhi_post_testresults() requires non-empty update, "\
                "testname, result and url arguments.")
        return False

    if not Config.fas_username or not Config.fas_password:
        log.error('Conf file containing FAS credentials is incomplete!')
        return False

    if result in ['ABORTED', 'CRASHED']:
        log.debug("Results with outcome of CRASHED or ABORTED are not submitted"
                  " to bodhi: %s %s" % (update, result))
        return True

    log.debug('Posting a result to Bodhi for: %s', update)

    comment = 'Taskotron: %s test %s on %s. Result log: %s ' \
              '(results are informative only)' % (testname, result, arch, url)
    try:
        bodhi_update = bodhi_api.query_update(update)

        (old_result, comment_time) = _already_commented(bodhi_update, testname,
                                                        arch)
        comment_needed = _is_comment_needed(old_result, comment_time, result)

        if not comment_needed and doreport != 'all':
            log.debug('Up-to-date test result is already present in Bodhi: %s %s',
                      update, result)
            return True

        parsed_results = []
        for found_comment in bodhi_update['comments']:
            if found_comment['author'] == Config.fas_username:
                parsed_results.append(_parse_result_from_comment(found_comment))

        kojitag = 'updates-testing'
        if bodhi_update['request'] == 'stable' or bodhi_update['status'] == 'stable':
            kojitag = 'updates'

        new_result = {'time':datetime.datetime.utcnow(), 'testname':testname,
                      'result':result, 'arch':arch}

        send_email = _is_comment_email_needed(update, parsed_results,
                new_result, kojitag)


        if not bodhi_api.client.comment(update, comment, karma, send_email):
            log.error('Could not post a comment to Bodhi. %r, %r, %r, %r',
                    update, comment, karma, send_email)
            return False

        log.debug('Test result successfully submitted to Bodhi: %s %s',
                  update, result)

    except Exception as e:
        log.exception(e)
        return False

    return True
