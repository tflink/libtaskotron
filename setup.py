from setuptools import setup, Command
import codecs
import re
import os

here = os.path.abspath(os.path.dirname(__file__))

def read(*parts):
    return codecs.open(os.path.join(here, *parts), 'r').read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


class PyTest(Command):
    user_options = []
    def initialize_options(self):
        pass
    def finalize_options(self):
        pass
    def run(self):
        import subprocess
        errno = subprocess.call(['py.test', 'testing'])
        raise SystemExit(errno)


setup(name='libtaskotron',
      version=find_version('libtaskotron', '__init__.py'),
      description='base lib for taskotron jobs',
      author='Tim Flink',
      author_email='tflink@fedoraproject.org',
      license='GPLv2+',
      url='https://bitbucket.org/fedoraqa/libtaskotron',
      packages=['libtaskotron', 'libtaskotron.directives'],
      package_dir={'libtaskotron':'libtaskotron'},
      include_package_data=True,
      cmdclass={'test': PyTest},
      entry_points=dict(console_scripts=['runtask=libtaskotron.runner:main']),
      install_requires = [
        'PyYAML',
      ]
)
