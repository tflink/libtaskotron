The linter's behaviour is configured in the .arclint file.

As the .arclint file parser does not support YAML comments, some documentation
can be put here if needed. Generic arcanist linter documentation is here:
https://secure.phabricator.com/book/phabricator/article/arcanist_lint/

Flake8
======
All PEP8 error codes (E* codes) are considered warnings in Phabricator, so that
only changes to the modified lines are displayed by default (unlike errors,
which are always displayed regardless of which lines were modified).

All PyFlakes error codes (F* codes) are considered errors, because they are
often introduced when modifying other lines than the one in question, and we
want to be notified of those.

Additional Flake8 configuration is stored in tox.ini - it contains configuration
which is not possible to do in .arclint file.

If you want to ignore a specific source code line, use '# noqa' comment. If you
want to ignore the whole file, add '# flake8: noqa' comment. Read more
documentation about flake8 at:
https://flake8.readthedocs.org/

