# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''A collection of convenience methods related to Python base libraries.'''

from __future__ import absolute_import
import collections

import libtaskotron.exceptions as exc


def iterable(obj, item_type=None):
    '''Decide whether ``obj`` is an :class:`~collections.Iterable` (you can
    traverse through its elements - e.g. ``list``, ``tuple``, ``set``, even
    ``dict``), but not ``basestring`` (which satisfies most collections'
    requirements, but we don't often want to consider as a collection). You can
    also verify the types of items in the collection.

    :param any obj: any object you want to check
    :param type item_type: all items in ``obj`` must be instances of this
                           provided type. If ``None``, no check is performed.
    :return: whether ``obj`` is iterable but not a string, and whether ``obj``
                     contains only ``item_type`` items
    :rtype: bool
    :raise TaskotronValueError: if you provide invalid parameter value
    '''
    return _collection_of(obj, collections.Iterable, item_type)


def sequence(obj, item_type=None, mutable=False):
    '''This has the same functionality and basic arguments as :func:`iterable`
    (read its documentation), but decides whether ``obj`` is a
    :class:`~collections.Sequence` (ordered and indexable collection - e.g.
    ``list`` or ``tuple``, but not ``set`` or ``dict``).

    :param bool mutable: if ``True``, the ``obj`` must be a mutable sequence
                         (e.g. ``list``, but not ``tuple``)
    '''
    col = collections.MutableSequence if mutable else collections.Sequence
    return _collection_of(obj, col, item_type)


def _collection_of(obj, collection_cls, item_type=None):
    '''The same as :func:`iterable` or :func:`sequence`, but the abstract
    collection class can be specified dynamically with ``collection_cls``.
    '''
    if not isinstance(obj, collection_cls) or isinstance(obj, basestring):
        return False

    if item_type is not None and isinstance(obj, collections.Iterable):
        try:
            return all([isinstance(item, item_type) for item in obj])
        except TypeError as e:
            raise exc.TaskotronValueError("'item_type' must be a type "
                "definition, not '%r': %s" % (item_type, e))

    return True
