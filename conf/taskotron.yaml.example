## Main configuration file for Taskotron
## The file is in YAML syntax, read more about it at:
## http://en.wikipedia.org/wiki/Yaml
## FIXME: add a link to Taskotron docs

## ==== GENERAL section ====
## There are two major config profiles in Taskotron - development and
## production.
## /Development/ profile is used for developing libtaskotron, developing checks
## based on libtaskotron and local execution of these checks.
## /Production/ profile is used for deploying Taskotron as a service on a
## server, periodically executing the checks and reporting results to relevant
## result consumers.
## *The default profile is /development/*. If you want to switch to the
## /production/ profile, uncomment the following line.
## You can also switch a profile temporarily by using TASKOTRON_PROFILE=name
## environment variable, it has a higher priority. All other options set in this
## file still apply of course.
## [choices: production, development; default: development]
#profile: production


## ==== REPORTING section ====
## This section controls which result reports you want to send after the test
## execution is complete

## Overall setting whether to send any reports at all. If this is False, no
## reports will be sent anywhere, regardless of your configuration for
## individual recipient systems.
## [default: True for production, False for development]
#reporting_enabled: True

## If True, test results (for selected tests) will be sent as comments to
## Fedora Update System (Bodhi). This requires that you have Bodhi
## credentials filled in secrets.conf. See also 'reporting_enabled' option.
#report_to_bodhi: True

## Whether to send test results to the configured ResultsDB server. See also
## 'reporting_enabled' option.
#report_to_resultsdb: True


## ==== RESOURCES section ====
## This section specifies access details to various external services.
##
## Note: Try to keep custom URL addresses without a trailing slash. Otherwise
## the rendered URLs might end up containing double slashes, which some
## application servers don't handle gracefully (e.g. Flask's internal app
## server werkzeug).

## URL of Koji instance used for querying about new builds
#koji_url: http://koji.fedoraproject.org/kojihub

## URL of repository of all the RPM packages built in Koji
#pkg_url: http://kojipkgs.fedoraproject.org/packages

## URL of Bodhi instance used for communication about package updates
## Please make sure the URL doesn't have a trailing slash.
#bodhi_server: https://admin.fedoraproject.org/updates
#bodhi_server: http://localhost:5000/bodhi

## URL of ResultsDB server API interface, which can store all test results.
## Please make sure the URL doesn't have a trailing slash.
#resultsdb_server: http://localhost:5001/api/v1.0

## URL of ResultsDB frontend, which displays results from ResultsDB.
## Please make sure the URL doesn't have a trailing slash.
#resultsdb_frontend: http://localhost:5002

## URL of ExecDB server API interface, which tracks task execution status.
## Please make sure the URL doesn't have a trailing slash.
#execdb_server: http://localhost:5003

## URL of taskotron buildmaster, to construct log URLs from.
## Please make sure the URL doesn't have a trailing slash.
#taskotron_master: http://localhost/taskmaster

## URL of artifacts base directory, to construct artifacts URLs from.
## Please make sure the URL doesn't have a trailing slash.
#artifacts_baseurl: http://localhost/artifacts

## name of step in buildbot that executes the task
#buildbot_task_step = 'runtask'

## Whether to cache downloaded files to speed up subsequent downloads. If True,
## files will be downloaded to a common directory specified by "cachedir". At
## the moment, Taskotron only supports Koji RPM downloads to be cached.
## [default: False for production, True for development]
#download_cache_enabled: False


## ==== BODHI EMAIL section ====
## These configuration options affect how Taskotron decideds to send emails
## through Bodhi in specific situations.

## How long (in minutes) should we wait before allowing consequent test to
## re-post a 'FAILED' comment into Bodhi once again.
## By default 3 days (3*24*60 = 4320).
#bodhi_posting_comments_span: 4320


## ==== PATHS section ====
## Location of various pieces of the project.

## The location of log files for Taskotron
#logdir: /var/log/taskotron

## The location of temporary files for Taskotron
#tmpdir: /var/tmp/taskotron

## The location of artifacts produced by checks
#artifactsdir: /var/lib/taskotron/artifacts

## The location of cached files downloaded by Taskotron
#cachedir: /var/cache/taskotron


## ==== LOGGING section ====
## This section contains configuration of logging

## Configuration of logging level. Here can be configured which messages
## will be logged. You can specify different level for logging to standard
## output (option log_level_stream) and logging to file (log_level_file).
## Possible values can be found here:
## https://docs.python.org/2.7/library/logging.html#logging-levels
#log_level_stream: INFO
#log_level_file: DEBUG

## If True, logging to file will be enabled.
## [default: True for production, False for development]
#log_file_enabled: True


## ==== SECRETS section ====
## All login credentials and other secrets are here. If you add some secret
## here, make sure you make this file readable just for the right user accounts.

## FAS (Fedora Accounts System) credentials
## These credentials are used when reporting results into Bodhi.
#fas_username: taskotron
#fas_password: password
