# -*- coding: utf-8 -*-
# Copyright 2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Functional tests for libtaskotron/file_utils.py'''

import os
import pytest
from dingus import Dingus
from urlgrabber.grabber import URLGrabError

from libtaskotron import file_utils
from libtaskotron import exceptions as exc


class TestSameLength():

    def test_nonexistent_file(self, tmpdir):
        '''Should not crash if the file doesn't exist'''
        fpath = tmpdir.join('nonexistent.file').strpath
        assert file_utils._same_length(fpath, None) is False


@pytest.mark.usefixtures('setup')
class TestDownload():

    @pytest.fixture
    def setup(self, tmpdir):
        '''Run this before every test method execution start'''
        self.dirname = tmpdir.join('download').strpath
        self.cachedir = tmpdir.join('cachedir').strpath
        self.stub_grabber = Dingus()

    def test_create_dirname(self, tmpdir):
        '''output dir should get created if it doesn't exist, multiple levels'''
        dirname = tmpdir.join('parentdir/subdir').strpath
        assert not os.path.exists(dirname)
        file_utils.download(url='http://localhost/file', dirname=dirname,
                            grabber=self.stub_grabber)
        assert os.path.isdir(dirname)

    def test_filename_missing(self):
        '''filename should be automatically derived'''
        file_utils.download(url='http://localhost/file.xyz', dirname=self.dirname,
                            grabber=self.stub_grabber)
        call = self.stub_grabber.calls[0]
        assert call[0] == 'urlgrab'
        assert call[2]['filename'] == os.path.join(self.dirname, 'file.xyz')

    def test_filename_provided(self):
        '''filename should be respected when provided'''
        file_utils.download(url='http://localhost/file.xyz', dirname=self.dirname,
                            filename='important.file', grabber=self.stub_grabber)
        call = self.stub_grabber.calls[0]
        assert call[0] == 'urlgrab'
        assert call[2]['filename'] == os.path.join(self.dirname, 'important.file')

    def test_create_cachedir(self):
        '''cachedir should be created if it doesn't exist'''
        assert not os.path.exists(self.cachedir)
        file_utils.download(url='http://localhost/file', dirname=self.dirname,
                            cachedir=self.cachedir, grabber=self.stub_grabber)
        assert os.path.isdir(self.cachedir)

    def test_cache_used(self):
        '''file should be downloaded to cache if cachedir is defined'''
        file_utils.download(url='http://localhost/file', dirname=self.dirname,
                            cachedir=self.cachedir, grabber=self.stub_grabber)
        call = self.stub_grabber.calls[0]
        assert call[0] == 'urlgrab'
        assert call[2]['filename'] == os.path.join(self.cachedir, 'file')

    def test_cache_not_used(self):
        '''file should not be downloaded to cache if cachedir is not defined'''
        file_utils.download(url='http://localhost/file', dirname=self.dirname,
                            grabber=self.stub_grabber)
        call = self.stub_grabber.calls[0]
        assert call[0] == 'urlgrab'
        assert not call[2]['filename'].startswith(self.cachedir)

    def test_skip_download_if_cached(self, tmpdir, monkeypatch):
        '''if the file is already in cache, it should not be downloaded again'''
        tmpdir.mkdir('download').join('file').write('data')
        stub_same_length = lambda *args, **kwargs: True
        monkeypatch.setattr(file_utils, '_same_length', stub_same_length)

        file_utils.download(url='http://localhost/file', dirname=self.dirname,
                            grabber=self.stub_grabber)
        assert len(self.stub_grabber.calls) == 0

    def test_create_symlink(self):
        '''symlink should be created if the download is cached'''
        file_utils.download(url='http://localhost/file', dirname=self.dirname,
                            cachedir=self.cachedir, grabber=self.stub_grabber)
        assert os.path.islink(os.path.join(self.dirname, 'file'))

    def test_skip_symlink(self):
        '''symlink should not be in dirname if the download is not cached'''
        file_utils.download(url='http://localhost/file', dirname=self.dirname,
                            grabber=self.stub_grabber)
        assert not os.path.islink(os.path.join(self.dirname, 'file'))

    def test_return_value(self):
        '''downloaded file path should be returned'''
        path = file_utils.download(url='http://localhost/file',
                                   dirname=self.dirname, grabber=self.stub_grabber)
        assert path == os.path.join(self.dirname, 'file')

    def test_raise(self):
        '''download errors should be raised'''
        def raise_urlgrab_error(*args, **kwargs):
            raise URLGrabError('fake download failed')
        stub_grabber = Dingus(urlgrab=raise_urlgrab_error)

        with pytest.raises(exc.TaskotronRemoteError):
            file_utils.download(url='http://localhost/file', dirname=self.dirname,
                                grabber=stub_grabber)
