# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Unit tests for libtaskotron/check.py'''

import pytest
import yaml
from copy import deepcopy

from libtaskotron import check
from libtaskotron import exceptions as exc

class TestCheckDetail(object):

    def setup_method(self, method):
        '''Run this before every test invocation'''
        # for most methods, we want a default empty CheckDetail instance
        self.cd = check.CheckDetail(None)

    def test_init_empty(self):
        '''Test instance creation with empty parameters'''
        # by default, outcome should be 'NEEDS_INSPECTION', not empty
        assert self.cd.outcome == 'NEEDS_INSPECTION'
        assert not self.cd.item
        assert not self.cd.summary
        assert not self.cd.output
        assert not self.cd.keyvals
        assert not self.cd.checkname

    def test_init_params(self):
        '''Test instance creation with non-empty parameters'''
        cd = check.CheckDetail(item='foo-1.2-3.fc20', outcome='FAILED',
                               summary='untested', output=['line1', 'line2'],
                               checkname='testcheck', artifact='logfile.log')
        assert cd.item == 'foo-1.2-3.fc20'
        assert cd.outcome == 'FAILED'
        assert cd.summary == 'untested'
        assert cd.output == ['line1', 'line2']
        assert cd.checkname == 'testcheck'
        assert cd.artifact == 'logfile.log'

    def test_init_raise(self):
        '''Test instance creation that raises an error - invalid parameters'''
        with pytest.raises(exc.TaskotronValueError):
            check.CheckDetail(None, outcome='INVALID TYPE')

        with pytest.raises(exc.TaskotronValueError):
            check.CheckDetail(None, output='foobar')

    def test_outcome(self):
        '''Test 'outcome' attribute'''
        # default value test
        # (already tested in constructor test, but it logically belongs here,
        # so let's include it again, in case constructor test changes)
        assert self.cd.outcome == 'NEEDS_INSPECTION'

        # test setter method with correct values
        self.cd.outcome = 'INFO'
        assert self.cd.outcome == 'INFO'

        self.cd.outcome = 'PASSED'
        assert self.cd.outcome == 'PASSED'

    def test_outcome_raise(self):
        '''Test 'outcome' attribute when it should raise an error'''
        with pytest.raises(exc.TaskotronValueError):
            self.cd.outcome = 'INVALID OUTCOME TYPE'

    def test_update_outcome(self):
        '''Test 'update_outcome' method'''
        # basic use case
        self.cd.update_outcome('FAILED')
        assert self.cd.outcome == 'FAILED'

        # update_outcome respects the outcome priority ordering, it never
        # overrides a more important outcome with a less important one;
        # therefore it should retain FAILED here
        self.cd.update_outcome('PASSED')
        assert self.cd.outcome == 'FAILED'

        # None should do nothing
        self.cd.update_outcome(None)
        assert self.cd.outcome == 'FAILED'

    def test_update_outcome_raise(self):
        '''Test 'update_outcome' method when it should raise an error'''
        with pytest.raises(exc.TaskotronValueError):
            self.cd.update_outcome('INVALID OUTCOME TYPE')

    def test_broken(self):
        '''Test 'broken' method'''
        # by default we're not broken
        assert not self.cd.broken()

        # two values should make it look broken
        self.cd.outcome = 'ABORTED'
        assert self.cd.broken()

        self.cd.outcome = 'CRASHED'
        assert self.cd.broken()

        # and we can go back to a non-broken state
        self.cd.outcome = 'FAILED'
        assert not self.cd.broken()

    def test_store(self, capsys):
        '''Test 'store' method'''
        # default use case where we fill 'output' and print to stdout as well
        self.cd.store('foobar')
        assert self.cd.output == ['foobar']
        out, err = capsys.readouterr()
        assert out == 'foobar\n'
        assert not err

        # use case without printing to stdout
        self.cd.store('barbaz', printout=False)
        assert self.cd.output == ['foobar', 'barbaz']
        out, err = capsys.readouterr()
        assert not out
        assert not err

    def test_cmp_outcome(self):
        '''Test 'cmp_outcome' method'''
        # sample comparisons where the first parameter is 'larger' (more
        # important) than the second one
        assert check.CheckDetail.cmp_outcome('FAILED', 'PASSED') == 1
        assert check.CheckDetail.cmp_outcome('NEEDS_INSPECTION', 'INFO') == 1
        assert check.CheckDetail.cmp_outcome('CRASHED', 'ABORTED') == 1
        assert check.CheckDetail.cmp_outcome('PASSED', None) == 1

        # sample comparisons where both the parameters are equal (same
        #importance)
        assert check.CheckDetail.cmp_outcome('PASSED', 'PASSED') == 0
        assert check.CheckDetail.cmp_outcome(None, None) == 0

        # sample comparisons where the first parameter is 'smaller' (less
        # important) than the second one
        assert check.CheckDetail.cmp_outcome('PASSED', 'FAILED') == -1
        assert check.CheckDetail.cmp_outcome(None, 'PASSED') == -1

    def test_cmp_outcome_raise(self):
        '''Test 'cmp_outcome' method when it should raise an error'''
        # test invalid outcome types as an input
        with pytest.raises(exc.TaskotronValueError):
            assert check.CheckDetail.cmp_outcome('PASSED', 'INVALID OUTCOME')

        with pytest.raises(exc.TaskotronValueError):
            assert check.CheckDetail.cmp_outcome('INVALID OUTCOME', 'FAILED')

        with pytest.raises(exc.TaskotronValueError):
            assert check.CheckDetail.cmp_outcome('FOOBAR', 'FOOBAR')

    def test_create_multi_item_summary(self):
        '''Test create_multi_item_summary method'''
        # a basic use case with a list of strings
        summary = check.CheckDetail.create_multi_item_summary(['PASSED',
                  'PASSED', 'FAILED', 'ABORTED', 'INFO', 'INFO'])
        assert summary == '2 PASSED, 2 INFO, 1 FAILED, 1 ABORTED'

        # an empty list should return an empty string
        summary = check.CheckDetail.create_multi_item_summary([])
        assert summary == ''

        # a use case with one CheckDetail instance input
        summary = check.CheckDetail.create_multi_item_summary(self.cd)
        assert summary == '1 NEEDS_INSPECTION'

        # a use case with multiple CheckDetail instances input
        cd1 = check.CheckDetail(None, outcome='PASSED')
        cd2 = check.CheckDetail(None, outcome='PASSED')
        cd3 = check.CheckDetail(None, outcome='INFO')
        summary = check.CheckDetail.create_multi_item_summary([self.cd,
                  cd1, cd2, cd3])
        assert summary == '2 PASSED, 1 INFO, 1 NEEDS_INSPECTION'

    def test_create_multi_item_summary_raise(self):
        '''Test create_multi_item_summary method when it should raise an
           error'''
        # a single string input is not allowed, only a list of them
        with pytest.raises(exc.TaskotronValueError):
            assert check.CheckDetail.create_multi_item_summary('PASSED')


class TestExportTAP(object):
    '''Test @method export_TAP'''
    item = 'xchat-0.5-1.fc20'
    outcome = 'PASSED'
    summary = '5 ERRORS, 10 WARNINGS'
    report_type = check.ReportType.KOJI_BUILD
    output = '''\
xchat.x86_64: W: file-not-utf8 /usr/share/doc/xchat/ChangeLog
xchat.x86_64: W: non-conffile-in-etc /etc/gconf/schemas/apps_xchat_url_handler.schemas
xchat.x86_64: W: no-manual-page-for-binary xchat'''
    keyvals = {"foo": "bar", "moo": 11}
    checkname = 'testcheck'
    artifact = 'logfile.log'
    _internal = {'resultsdb_result_id': 1234}

    def setup_method(self, method):
        self.cd = check.CheckDetail(item=self.item, outcome=self.outcome,
                                    summary=self.summary,
                                    report_type=self.report_type,
                                    keyvals=self.keyvals,
                                    checkname=self.checkname,
                                    artifact=self.artifact)
        self.cd._internal = self._internal
        self.cd.output.append(self.output)

    def _tap_to_yaml(self, rendered_tap):
        '''Remove all TAP-related contents and return only its YAML block(s)'''
        lines = rendered_tap.splitlines()[2:]

        # split the output to TAP result lines and YAML blocks
        tap_lines = [line for line in lines if line.startswith('ok') or
                                               line.startswith('not ok')]
                      # remove left padding - 2 spaces
        yaml_lines = [line[2:] for line in lines if line not in tap_lines]

        return '\n'.join(yaml_lines)

    def test_single_tap(self):
        '''Test export with a single item section. Check generic TAP lines.'''
        tap_output = check.export_TAP(self.cd)
        lines = tap_output.splitlines()

        # generic lines must exist
        assert lines[0].startswith('TAP version 13')
        assert lines[1].startswith('1..')
        assert lines[2].startswith('ok ')
        assert lines[3].strip() == '---'
        assert lines[-1].strip() == '...'

    def test_single_yaml(self):
        '''Test export with a single item section. Check specific YAML lines.'''
        tap_output = check.export_TAP(self.cd)

        # specific lines must exist
        yaml_block = self._tap_to_yaml(tap_output)
        yaml_obj = yaml.safe_load(yaml_block)

        assert yaml_obj['item'] == self.item
        assert yaml_obj['outcome'] == self.outcome
        assert yaml_obj['summary'] == self.summary
        assert yaml_obj['type'] == self.report_type
        assert yaml_obj['details']
        assert yaml_obj['details']['output'] == self.output
        assert yaml_obj['foo'] == self.keyvals['foo']
        assert yaml_obj['moo'] == self.keyvals['moo']
        assert yaml_obj['checkname'] == self.checkname
        assert yaml_obj['_internal'] == self._internal

    def test_invalid_keyvals(self):
        '''Test export with keyvals containing reserved keys.'''
        cd = deepcopy(self.cd)

        for key in check.RESERVED_KEYS:
            cd.keyvals[key] = 'foo'

        tap_output = check.export_TAP(cd)

        # specific lines must exist
        yaml_block = self._tap_to_yaml(tap_output)
        yaml_obj = yaml.load(yaml_block)

        assert yaml_obj['item'] == self.item
        assert yaml_obj['outcome'] == self.outcome
        assert yaml_obj['summary'] == self.summary
        assert yaml_obj['type'] == self.report_type
        assert yaml_obj['details']
        assert yaml_obj['details']['output'] == self.output
        assert yaml_obj['foo'] == self.keyvals['foo']
        assert yaml_obj['moo'] == self.keyvals['moo']
        assert yaml_obj['checkname'] == self.checkname
        assert yaml_obj['_internal'] == self._internal

    def test_multi(self):
        '''Test export with multiple item sections'''
        cd2 = check.CheckDetail(item='foobar-1.2-3.fc20', outcome='FAILED',
                                summary='dependency error',
                                report_type=check.ReportType.BODHI_UPDATE)
        cd3 = check.CheckDetail(item='f20-updates', outcome='INFO',
                                summary='2 stale updates',
                                report_type=check.ReportType.YUM_REPOSITORY)
        tap_output = check.export_TAP([self.cd, cd2, cd3])
        lines = tap_output.splitlines()

        assert lines[1].strip() == '1..3'

        yaml_blocks = self._tap_to_yaml(tap_output)

        # test that we have a correct number of TAP lines
        tap_lines = [line for line in lines if line.startswith('ok') or
                                               line.startswith('not ok')]
        assert len(tap_lines) == 3

        # test that we have a correct number (and rough contents) of YAML blocks
        yaml_gen = yaml.safe_load_all(yaml_blocks)
        for index, yaml_obj in enumerate(yaml_gen):
            assert 'item' in yaml_obj
        assert index == 2

    def test_invalid(self):
        '''Test invalid input parameters'''
        with pytest.raises(exc.TaskotronValueError):
            self.cd.item = None
            check.export_TAP(self.cd)

    def test_minimal(self):
        '''Test that empty CheckDetail values don't produce empty TAP lines,
        for example 'summary:' should not be present if there's no
        CheckDetail.summary'''
        cd = check.CheckDetail('foo')
        tap_output = check.export_TAP(cd)
        yaml_block = self._tap_to_yaml(tap_output)
        yaml_obj = yaml.safe_load(yaml_block)

        # the output should look like this:
        #
        # TAP Version 13
        # 1..1
        # ok - $CHECKNAME for XXX
        # ---
        # item: XXX
        # outcome: XXX
        # ...
        #
        # ('outcome:' is technically not a mandatory line, but with CheckDetail
        # export we produce it every time)
        assert len(yaml_obj) == 2

    def test_tap_ok(self):
        '''Test that TAP header contains 'ok' for appropriate outcomes'''
        # by default the outcome is PASSED
        assert check.export_TAP(self.cd).splitlines()[2].startswith('ok')
        self.cd.outcome = 'INFO'
        assert check.export_TAP(self.cd).splitlines()[2].startswith('ok')

    def test_tap_not_ok(self):
        '''Test that TAP header contains 'not ok' for appropriate outcomes'''
        self.cd.outcome = 'FAILED'
        assert check.export_TAP(self.cd).splitlines()[2].startswith('not ok')
        self.cd.outcome = 'NEEDS_INSPECTION'
        assert check.export_TAP(self.cd).splitlines()[2].startswith('not ok')
        self.cd.outcome = 'ABORTED'
        assert check.export_TAP(self.cd).splitlines()[2].startswith('not ok')
        self.cd.outcome = 'CRASHED'
        assert check.export_TAP(self.cd).splitlines()[2].startswith('not ok')

    def test_custom_report_type(self):
        '''Test that user can specify a custom CheckDetail.report_type'''
        self.cd.report_type = 'My Custom Type'
        tap_output = check.export_TAP(self.cd)
        yaml_block = self._tap_to_yaml(tap_output)
        yaml_obj = yaml.safe_load(yaml_block)

        assert yaml_obj['type'] == self.cd.report_type

    def test_checkname(self):
        '''Test that replacing $CHECKNAME works'''
        # by default the outcome is PASSED
        output = check.export_TAP(self.cd)
        assert "ok - $CHECKNAME for" in output

        output = check.export_TAP(self.cd, "my_beautifull_check_name")
        assert "ok - $CHECKNAME for" not in output
        assert "ok - my_beautifull_check_name for" in output


class TestImportTAP(object):
    '''Test @method import_TAP'''
    item = 'xchat-0.5-1.fc20'
    outcome = 'PASSED'
    summary = '5 ERRORS, 10 WARNINGS'
    report_type = check.ReportType.KOJI_BUILD
    output = '''\
xchat.x86_64: W: file-not-utf8 /usr/share/doc/xchat/ChangeLog
xchat.x86_64: W: non-conffile-in-etc /etc/gconf/schemas/apps_xchat_url_handler.schemas
xchat.x86_64: W: no-manual-page-for-binary xchat'''
    keyvals = {"foo": "bar", "moo": 11}
    checkname = 'testcheck'
    artifact = 'logfile.log'
    _internal = {'resultsdb_result_id': 1234}

    tap_output = """
TAP version 13
1..1
ok - $CHECKNAME for Koji build xchat-0.5-1.fc20
    ---
    details:
        output: |-
            xchat.x86_64: W: file-not-utf8 /usr/share/doc/xchat/ChangeLog
            xchat.x86_64: W: non-conffile-in-etc /etc/gconf/schemas/apps_xchat_url_handler.schemas
            xchat.x86_64: W: no-manual-page-for-binary xchat
    item: xchat-0.5-1.fc20
    outcome: PASSED
    artifact: logfile.log
    summary: 5 ERRORS, 10 WARNINGS
    type: koji_build
    foo: bar
    moo: 11
    checkname: testcheck
    _internal:
        resultsdb_result_id: 1234
    ..."""

    def setup_method(self, method):
        self.cd = check.CheckDetail(item=self.item, outcome=self.outcome,
                                    summary=self.summary,
                                    report_type=self.report_type,
                                    keyvals=self.keyvals,
                                    checkname=self.checkname,
                                    artifact=self.artifact)
        self.cd._internal = self._internal
        self.cd.output.append(self.output)

    def test_import(self):
        cds = check.import_TAP(self.tap_output)

        assert len(cds) == 1
        assert isinstance(cds[0], check.CheckDetail)

        cd = cds[0]
        assert cd.item == self.cd.item
        assert cd.outcome == self.cd.outcome
        assert cd.summary == self.cd.summary
        assert cd.report_type == self.cd.report_type
        assert cd.output == self.cd.output
        assert cd.keyvals == self.keyvals
        assert cd.checkname == self.checkname
        assert cd.artifact == self.artifact
        assert cd._internal == self._internal

    def test_import_exported(self):
        tap_output = check.export_TAP(self.cd)

        cds = check.import_TAP(tap_output)

        assert len(cds) == 1
        assert isinstance(cds[0], check.CheckDetail)

        cd = cds[0]
        assert cd.item == self.cd.item
        assert cd.outcome == self.cd.outcome
        assert cd.summary == self.cd.summary
        assert cd.report_type == self.cd.report_type
        assert cd.output == self.cd.output
        assert cd.keyvals == self.keyvals
        assert cd.checkname == self.checkname
        assert cd.artifact == self.artifact
        assert cd._internal == self._internal


    def test_invalid_tap(self):
        tap_output = """
TAP version 13
ok 2 foo
ok 1 bar"""
        with pytest.raises(exc.TaskotronValueError):
            check.import_TAP(tap_output)

    def test_invalid_tap_multi_header(self):
        #simplified test data from phab/T205
        tap_output_multi_header = """
TAP version 13
1..1
ok - $CHECKNAME for XXX
---
item: XXX
...
TAP version 13
1..1
ok - $CHECKNAME for YYY
---
item: YYY
...
"""
        with pytest.raises(ValueError):
            check.import_TAP(tap_output_multi_header)

    def test_no_yaml(self):
        tap_output = '''
TAP version 13
1..1
ok'''
        cds = check.import_TAP(tap_output)
        assert len(cds) == 1
        assert isinstance(cds[0], check.CheckDetail)

        cd = cds[0]
        assert not cd.item
        assert cd.outcome == 'PASSED'
        assert not cd.summary
        assert not cd.report_type
        assert not cd.output
        assert not cd.keyvals
        assert not cd.artifact
        assert not cd._internal


    def test_empty_yaml(self):
        tap_output = '''
TAP version 13
1..1
not ok
---
...'''
        cds = check.import_TAP(tap_output)
        assert len(cds) == 1
        assert isinstance(cds[0], check.CheckDetail)

        cd = cds[0]
        assert not cd.item
        assert cd.outcome == 'FAILED'
        assert not cd.summary
        assert not cd.report_type
        assert not cd.keyvals
        assert not cd.output
        assert not cd.checkname
        assert not cd.artifact
        assert not cd._internal
