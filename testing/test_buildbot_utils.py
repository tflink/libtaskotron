from dingus import Dingus

from libtaskotron import buildbot_utils

class TestBuildbotUtils(object):

    def setup_method(self, method):
        self.default_builder = 'default'
        self.default_buildid = '-1'

    def test_valid_jobid_parse(self):
        ref_builder = 'builder'
        ref_buildid = '1234'
        ref_jobid = '%s/%s' % (ref_builder, ref_buildid)

        test_builder, test_buildid = buildbot_utils.parse_jobid(ref_jobid)

        assert test_builder == ref_builder
        assert test_buildid == ref_buildid

    def test_default_jobid_parse(self):
        ref_jobid = '-1'

        test_builder, test_buildid = buildbot_utils.parse_jobid(ref_jobid)

        assert test_builder == self.default_builder
        assert test_buildid == self.default_buildid

    def test_invalid_jobid_parse_small(self):
        ref_jobid = 'crammalamma'

        test_builder, test_buildid = buildbot_utils.parse_jobid(ref_jobid)

        assert test_builder == self.default_builder
        assert test_buildid == self.default_buildid

    def test_invalid_jobid_parse_long(self):
        ref_jobid = 'crammalamma/swiss/cake/roll'

        test_builder, test_buildid = buildbot_utils.parse_jobid(ref_jobid)

        assert test_builder == self.default_builder
        assert test_buildid == self.default_buildid

    def test_invalid_jobid_parse_baddelim(self):
        ref_jobid = 'builder,1234'

        test_builder, test_buildid = buildbot_utils.parse_jobid(ref_jobid)

        assert test_builder == self.default_builder
        assert test_buildid == self.default_buildid

    def test_get_urls(self):
        ref_taskotron_master = 'http://taskotron-stg.fedoraproject.org/taskmaster/'
        ref_task_stepname = 'runtask'
        ref_builder = 'all'
        ref_buildid = '1234'
        ref_jobid = '%s/%s' % (ref_builder, ref_buildid)
        job_url, log_url = buildbot_utils.get_urls(ref_jobid, ref_taskotron_master, ref_task_stepname)

        assert job_url == '%s/builders/%s/builds/%s' % (ref_taskotron_master, ref_builder, ref_buildid)
        assert log_url == '%s/steps/%s/logs/stdio' % (job_url, ref_task_stepname)
